aGov Information Publication Scheme
===================================

This module provides a way to publish Australian Government Information Publication Scheme documents.

"The Information Publication Scheme (IPS) applies to Australian Government agencies that are subject to the Freedom of Information Act 1982 (FOI Act)"

Further details on IPS is available at the Office of the Information Commissioner site: http://www.oaic.gov.au/freedom-of-information/freedom-of-information-act...

This is a Features module that provides a Information Publication Scheme content type with required fields.
